#include "pch.h"
#include <mpi.h>
#include <cstring>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <unordered_set>
#include <omp.h>
#include <fstream>
#include <ctime>

using namespace std;


vector<int> lps;

void computeLPSArray(string pat);

// Prints occurrences of txt[] in pat[] 
vector<int> KMPSearch(const string& pat, string txt)
{
    int M = pat.length();
    int N = txt.length();
    int i = 0; // index for txt[] 
    int j = 0; // index for pat[] 
    vector<int> positions;
    while (i < N) {
        if (pat[j] == txt[i]) {
            j++;
            i++;
        }

        if (j == M) {
            positions.push_back(i - j);
            j = lps[j - 1];
        }

        // mismatch after j matches 
        else if (i < N && pat[j] != txt[i]) {
            if (j != 0)
                j = lps[j - 1];
            else
                i = i + 1;
        }
    }
    return positions;
}



// Fills lps[] for given patttern pat[0..M-1] 
void computeLPSArray(string pat)
{
    lps = vector<int>(pat.size());
    // length of the previous longest prefix suffix 
    int len = 0;

    lps[0] = 0; // lps[0] is always 0 

    // the loop calculates lps[i] for i = 1 to M-1 
    int i = 1;
    while (i < pat.size()) {
        if (pat[i] == pat[len]) {
            len++;
            lps[i] = len;
            i++;
        }
        else // (pat[i] != pat[len]) 
        {
            if (len != 0) {
                len = lps[len - 1];
            }
            else // if (len == 0) 
            {
                lps[i] = 0;
                i++;
            }
        }
    }
}



void KmpParallel(const string& text, string pat, int chunk_size, int& res_size, int* buf) {
    int proc_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
    int proc_num;
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    unordered_set<int> proc_res;
    int cur_pos = proc_id * chunk_size;
    vector<int> cur_res;
    if (proc_id == proc_num - 1) {
        cur_res = KMPSearch(pat, text.substr(proc_id * chunk_size));
    }
    else {
        cur_res = KMPSearch(pat, text.substr(proc_id * chunk_size, chunk_size));
    }
    for (auto cr : cur_res) {
        proc_res.insert(cr + cur_pos);
    }
    cur_pos = (proc_id + 1) * chunk_size - pat.size();
    if (proc_id != proc_num - 1) {
        string left_chunk, right_chunk;
		left_chunk = text.substr((proc_id + 1)* chunk_size - pat.size());
		right_chunk = text.substr((proc_id + 1) * chunk_size, pat.size());
		string border = left_chunk + right_chunk;
        cur_res = KMPSearch(pat, border);
        for (auto cr : cur_res) {
            proc_res.insert(cr + cur_pos);
        }
    }
    //buf = new int[proc_res.size()];
    int i = 0;
    for (auto r : proc_res) {
        buf[i] = r;
        ++i;
    }
    res_size = proc_res.size();
}

string RandomText(int length) {
    string rs; srand(time(NULL));
    for (int i = 0; i < length; ++i) {
        rs += abs(char(rand() % 26 + 97));
    }
    return rs;
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int text_size = 100000000;
    char* txt = new char[text_size];
   // char txt[] = "TextAText";
    string pat = "ta";
    computeLPSArray(pat);
    int num_threads;
    MPI_Comm_size(MPI_COMM_WORLD, &num_threads);
    int proc;
    int chunk_size = text_size / num_threads;
    MPI_Comm_rank(MPI_COMM_WORLD, &proc);
    double mpi_start, mpi_end;
    if (proc == 0) {
        string rnd_txt = RandomText(text_size);
        for (int i = 0; i < rnd_txt.size(); ++i) {
            txt[i] = rnd_txt[i];
        }
		
    }
    MPI_Bcast(txt, text_size, MPI_CHAR, 0, MPI_COMM_WORLD);

    int size;
    int other_size = 0;
    int* buf = new int[10000000];
    int* other_buf = new int[10000000];
	int* size_buf = new int[num_threads];
    unordered_set<int> res;
	MPI_Barrier(MPI_COMM_WORLD);
	if (proc == 0) mpi_start = omp_get_wtime();
    KmpParallel(txt, pat, chunk_size, size, buf);
	MPI_Barrier(MPI_COMM_WORLD);
	if (proc == 0) mpi_end = omp_get_wtime();
	int* displ = new int[num_threads];
	MPI_Gather(&size, 1, MPI_INT, size_buf, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if (proc == 0) {
		for (int i = 0; proc == 0 && i < num_threads - 1; ++i) {
			displ[i] = size_buf[i];
		}
	}
	MPI_Gatherv(buf, size, MPI_INT, other_buf, size_buf, size_buf, MPI_INT, 0, MPI_COMM_WORLD);
	if (proc == 0) {
		for (int i = 0; i < other_size; ++i) {
			res.insert(other_buf[i]);
		}
		for (int i = 0; i < size; ++i) {
			res.insert(buf[i]);
		}
		printf("Text length: %d \n", text_size);
		printf("MPI work took % f seconds with %d results\n", mpi_end - mpi_start, res.size());
	}
    MPI_Finalize();
}